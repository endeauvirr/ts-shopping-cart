import { createStore } from 'redux';
import enhancer from '../middlewares';
import reducers from '../reducers';


const store = createStore(reducers, enhancer);

export default store;