import * as React from 'react';

import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { updateBuyerData } from "../../actions/cart";
import { updateCartProgress } from "../../actions/cartProgress";

import Layout from '../Layout/Layout';
import ShippingForm from '../../components/ShippingForm/ShippingForm';
import ICart from '../../models/Cart';
import ICartProgress from "../../models/CartProgress";
import IShippingProvider from "../../models/ShippingProvider";


interface IProps {
	cart: ICart,
	history: any,
	cartProgress: ICartProgress,
	shippingProviders: IShippingProvider[],
	updateCartProgressAction(ICartProgress),
	updateBuyerDataAction(IBuyer)
}


class Shipping extends React.Component<IProps, {}> {
	public render() {
		const { shippingProviders, cart, updateBuyerDataAction, history, cartProgress } = this.props;
		
		const { cartConfirmed } = cartProgress;
		
		if (!cartConfirmed) {
			return (
				<Redirect to='/cart' />
			)
		}
		
		return (
			<Layout stepNumber={2} title={'Shipping'} >
				<ShippingForm
					shippingProviders={shippingProviders}
					freeShippingAvailable={cart.freeShippingAvailable}
					updateBuyerData={updateBuyerDataAction}
					handleProceedToNextStep={this.handleProceedToNextStep}
					history={history}
					buyer={cart.buyer}
				/>
			</Layout>
		)
	}
	private handleProceedToNextStep = () => {
		const { updateCartProgressAction, history } = this.props;
		
		updateCartProgressAction({
			cartConfirmed: true,
			shippingConfirmed: true
		});
		
		history.push('/payment');
	}
}

const mapStateToProps = ({ cart, shippingProviders, cartProgress }) => {
	return {
		cart,
		cartProgress,
		shippingProviders
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		updateBuyerDataAction: (buyer) => dispatch(updateBuyerData(buyer)),
		updateCartProgressAction: (status) => dispatch(updateCartProgress(status)),
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Shipping);