import * as React from 'react';

import * as styles from './Cart.scss';
import { connect } from 'react-redux';

import { pendingFetchProducts, updateCartValue } from "../../actions/cart";
import { setInitialShippingProvider } from "../../actions/shippingProviders";
import { updateCartProgress } from "../../actions/cartProgress";

import ICart from '../../models/Cart';
import IProduct from '../../models/Product';

import Layout from '../Layout/Layout';
import Products from '../../components/Products/Products';
import ProductSkeleton from '../../components/ProductSkeleton/ProductSkeleton';
import CartSummary from '../../components/CartSummary/CartSummary';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSadTear } from '@fortawesome/free-solid-svg-icons';


interface IState {
	loading: boolean
}

interface IProps {
	cart: ICart,
	history: any,
	updateCartValueAction(products: IProduct[]),
	setInitialShippingProviderAction(products: IProduct[]),
	updateCartProgressAction(ICartProgress),
	pendingFetchProductsAction()
}


class Cart extends React.Component<IProps, IState> {
	public state = {
		loading: true
	}
	public componentDidUpdate(prevProps) {
		const { products } = this.props.cart;
		
		if (prevProps.cart.products.length !== products.length) {
			this.setState({
				loading: false
			})
		}
	}
	public componentDidMount() {
		const { pendingFetchProductsAction } = this.props;
		const { areProductsFetched, products } = this.props.cart;
		
		if (!areProductsFetched) {
			pendingFetchProductsAction();
		}
		
		if (products.length !== 0) {
			this.setState({ loading: false })
		}
	}
	public render() {
		const { loading } = this.state;
		const { products } = this.props.cart;
		
		return (
			<Layout stepNumber={1} title={'Product list'} >
				{
					loading ? (
						
						<ProductSkeleton />
					
					) : (
						<React.Fragment>
							{
								products.length === 0 ? (
									<p className={styles.cartEmpty}>
										Your cart is empty <FontAwesomeIcon icon={faSadTear} size="lg"/>
									</p>
								) : (
									<React.Fragment>
										<Products products={products}/>
										<CartSummary products={products} />
									</React.Fragment>
								)
							}
							
							<footer className={styles.footerWrapper}>
								{
									products.length !== 0 &&
									<button type="button" onClick={this.handleProceedToNextStep}>
										Buy
									</button>
								}
							</footer>
						</React.Fragment>
					)
				}
			
			</Layout>
		)
	}
	private handleProceedToNextStep = () => {
		const { cart, history, updateCartProgressAction, updateCartValueAction, setInitialShippingProviderAction } = this.props;
		
		updateCartValueAction(cart.products);
		setInitialShippingProviderAction(cart.products);
		
		updateCartProgressAction({
			cartConfirmed: true,
			shippingConfirmed: false
		});
		
		history.push('/shipping');
	}
}


const mapStateToProps = ({ cart }) => {
	return {
		cart
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		pendingFetchProductsAction: () => dispatch(pendingFetchProducts()),
		updateCartProgressAction: (status) => dispatch(updateCartProgress(status)),
		updateCartValueAction: (products) => dispatch(updateCartValue(products)),
		setInitialShippingProviderAction: (products) => dispatch(setInitialShippingProvider(products))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);