import * as React from 'react';

import * as styles from './Layout.scss';

const Layout = (props) => {
	const { stepNumber, title } = props;
	return (
		<section className={styles.container}>
			<header className={styles.headerWrapper}>
				<h3>
					<span className={styles.label}>step.{stepNumber}</span> {title}
				</h3>
			</header>
			
			{props.children}
		
		</section>
	);
};

export default Layout;