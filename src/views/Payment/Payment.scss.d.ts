export const selectControl: string;
export const selectControlArrow: string;
export const paymentWrapper: string;
export const paymentProviders: string;
export const buyerAgreement: string;
export const paymentFooter: string;
