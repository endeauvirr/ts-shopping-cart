import * as React from 'react';

import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';

import { formatPrice } from "../../utils/helpers";

import { setPaymentProvider } from "../../actions/paymentProviders";

import * as styles from './Payment.scss';
import * as formStyles from '../../assets/scss/_form-elements.scss';
import classNames from "classnames/bind";

import Layout from '../Layout/Layout';
import ICart from "../../models/Cart";
import IShippingProvider from "../../models/ShippingProvider";
import IPaymentProvider from "../../models/PaymentProvider";
import ICartProgress from "../../models/CartProgress";


interface IState {
	agreementsChecked: boolean
}

interface IProps {
	cart: ICart,
	shippingProviders: IShippingProvider[],
	paymentProviders: IPaymentProvider[],
	cartProgress: ICartProgress,
	setPaymentProviderAction(id: number)
}


const cx = classNames.bind({...formStyles, ...styles});


class Payment extends React.Component<IProps, IState> {
	public state = {
		agreementsChecked: false
	}
	public render() {
		const { paymentProviders, cartProgress } = this.props;
		const { cartConfirmed, shippingConfirmed } = cartProgress;
		
		if ( !cartConfirmed || !shippingConfirmed ) {
			return (
				<Redirect to='/cart' />
			)
		}
		
		const { agreementsChecked } = this.state;
		const { cartValue, freeShippingAvailable, buyer } = this.props.cart;
		
		
		const selectedShippingProvider = this.getShippingProviderData();
		const selectedPaymentProvider = this.getSelectedPaymentProvider();
		const productsQuantity = this.getProductsQuantity();
		
		const summaryCartValue = (freeShippingAvailable ? cartValue : cartValue + selectedShippingProvider.value);
		
		const formattedCartValue = formatPrice(summaryCartValue);
		
		return (
			<Layout stepNumber={3} title={'Payment'} >
				<div className={styles.paymentWrapper}>
					<h2>Summary</h2>
					<p>
						You will buy <strong>{productsQuantity}</strong> {(productsQuantity === 1) ? 'product': 'products'} for { formattedCartValue }&nbsp;
						delivered by <strong>{selectedShippingProvider.name}</strong> { freeShippingAvailable ? '(free delivery)' : '(delivery costs included)'}.
					</p>
					
					<hr/>
					
					<h3>Address</h3>
					<p>
						{buyer.name}
						<br />{buyer.address}
						{
							!!buyer.phone &&
							<React.Fragment>
								<br />+48 {buyer.phone}
							</React.Fragment>
						}
						<br />{buyer.email}
					</p>
					
					
					<div className={styles.paymentProviders}>
						<label htmlFor="paymentProviders">Payment method: </label>
						
						<div className={styles.selectControl}>
							<select name="paymentProviders" defaultValue={selectedPaymentProvider} onChange={this.setPaymentProvider}>
								{
									paymentProviders.map(provider => {
										return (
											!provider.disabled &&
											<option
												key={provider.id}
												value={provider.id}
											>
												{provider.name}
											</option>
										)
									})
								}
							</select>
							<div className={cx('selectControlArrow')}>&nbsp;</div>
						</div>
					
					</div>
					
					
					<div className={styles.buyerAgreement}>
						<input type="checkbox" id="buyerAgreement" name="buyerAgreement" onChange={this.setBuyerAgreement} />
						<label htmlFor="buyerAgreement">I read and accept regulations. * (mandatory agreement)</label>
					</div>
					
					
					<footer className={styles.paymentFooter}>
						<Link to='/shipping'>back to shipping</Link>
						<button type="button" disabled={!agreementsChecked} >
							Pay
						</button>
					</footer>
				
				</div>
			</Layout>
		)
	}
	private setBuyerAgreement = () => {
		this.setState((prevState) => ({
			agreementsChecked: !prevState.agreementsChecked
		}))
	}
	private getSelectedPaymentProvider = () => {
		const { paymentProviders } = this.props;
		
		return paymentProviders.filter(provider => provider.selected).shift().id;
	}
	private setPaymentProvider = (event) => {
		const { setPaymentProviderAction } = this.props;
		
		setPaymentProviderAction(Number(event.currentTarget.value))
	}
	private getShippingProviderData = () => {
		const { buyer } = this.props.cart;
		const { shippingProviders } = this.props;
		
		return shippingProviders.filter(provider => {
			return provider.id === Number(buyer.shippingProviderId)
		}).shift();
	}
	private getProductsQuantity = () => {
		const { products } = this.props.cart;
		
		return products.reduce((prev, current) => prev + current.quantity, 0);
	}

}


const mapStateToProps = ({ cart, shippingProviders, paymentProviders, cartProgress }) => {
	return {
		cart,
		shippingProviders,
		paymentProviders,
		cartProgress
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		setPaymentProviderAction: (id) => dispatch(setPaymentProvider(id))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);