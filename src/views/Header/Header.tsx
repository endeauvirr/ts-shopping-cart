import * as React from 'react';
import * as styles from './Header.scss';

import { Link } from 'react-router-dom';

import logo from '../../assets/images/logo.png';
import logoRetina from '../../assets/images/logo@retina.png';

const Header = () => {
	return (
		<header className={styles.headerWrapper}>
			<Link to={'/'} className={styles.logoWrapper}>
				<picture>
					<img srcSet={`${logo}, ${logoRetina} 2x`} alt="NinjaCart" />
				</picture>
				<p className={styles.logoText}>
					NinjaCart
				</p>
			</Link>
		</header>
	);
};

export default Header;