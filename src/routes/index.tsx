import * as React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import asyncComponent from '../components/AsyncComponent';

import Header from '../views/Header/Header';

const AsyncCart = asyncComponent(() => import('../views/Cart/Cart'));
const AsyncShipping = asyncComponent(() => import('../views/Shipping/Shipping'));
const AsyncPayment = asyncComponent(() => import('../views/Payment/Payment'));

const AppRouter = () => {
	return (
		<Router>
			<React.Fragment>
				<Header />
				
				<Switch>
					<Route path='/' exact={true} render={() => (<Redirect to='/cart' />)} />
					<Route path='/cart' exact={true} component={AsyncCart} />
					<Route path='/shipping' exact={true} component={AsyncShipping} />
					<Route path='/payment' exact={true} component={AsyncPayment} />
					<Route render={() => (<Redirect to='/cart' />)} />
				</Switch>
			</React.Fragment>
		</Router>
	)
};

export default AppRouter;