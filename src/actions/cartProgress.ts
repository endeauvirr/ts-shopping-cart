import ICartProgress from "../models/CartProgress";
import { ActionTypes } from "../utils/constants";

export interface IUpdateCartProgress {
	type: ActionTypes.UPDATE_CART_PROGRESS,
	status: ICartProgress
}

export type CartProgressActionTypes = IUpdateCartProgress;


export const updateCartProgress = (status: ICartProgress): IUpdateCartProgress => ({
	type: ActionTypes.UPDATE_CART_PROGRESS,
	status
});