import { ActionTypes } from "../utils/constants";

export interface ISetPaymentProvider {
	type: ActionTypes.SET_PAYMENT_PROVIDER,
	id: number
}

export type PaymentProvidersActionTypes = ISetPaymentProvider;


export const setPaymentProvider = (id: number): ISetPaymentProvider => ({
	type: ActionTypes.SET_PAYMENT_PROVIDER,
	id
});