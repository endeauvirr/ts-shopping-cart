import { v4 as uuid } from 'uuid';
import IBuyer from "../models/Buyer";
import IProduct from "../models/Product";
import api from '../utils/api';
import { ActionTypes } from '../utils/constants';
import { putMessage } from "./messenger";



export interface IFetchProducts {
	type: ActionTypes.FETCH_PRODUCTS,
	products: IProduct[]
}
export interface IIncreaseQuantity {
	type: ActionTypes.INCREASE_QUANTITY,
	product: {
		id: string
	}
}
export interface IDecreaseQuantity {
	type: ActionTypes.DECREASE_QUANTITY,
	product: {
		id: string
	}
}
export interface IChangeQuantity {
	type: ActionTypes.CHANGE_QUANTITY,
	product: {
		id: string,
		quantity: string
	}
}
export interface IRemoveFromCart {
	type: ActionTypes.REMOVE_FROM_CART,
	product: {
		id: string
	}
}
export interface IUpdateCartValue {
	type: ActionTypes.UPDATE_CART_VALUE,
	products: IProduct[]
}
export interface IUpdateBuyerData {
	type: ActionTypes.UPDATE_BUYER_DATA,
	buyer: IBuyer
}


export type CartActionTypes = IFetchProducts | IIncreaseQuantity | IDecreaseQuantity | IChangeQuantity
	| IRemoveFromCart | IUpdateBuyerData | IUpdateCartValue;





export const fetchProducts = (products: IProduct[]): IFetchProducts => ({
	type: ActionTypes.FETCH_PRODUCTS,
	products
});

export const pendingFetchProducts = () => {
	return (dispatch: any) => {
		fetch(api.fetchProductsURL)
			.then(response => {
				const { status } = response;

				if (status === 200 || status === 201) {
					return response.json()
				}

				dispatch(putMessage({
					id: uuid(),
					message: 'We could not get your purchases. Please try refresh the page!',
					type: 'error'
				}));

				return [];
			})
			.then(products => {
				dispatch(fetchProducts(products))
			})
			.catch(error => {
				throw new Error(`Error while fetching products: ${error}`);
			})
	}
};

export const increaseQuantity = (id: string): IIncreaseQuantity => ({
	type: ActionTypes.INCREASE_QUANTITY,
	product: {
		id
	}
});

export const decreaseQuantity = (id: string): IDecreaseQuantity => ({
	type: ActionTypes.DECREASE_QUANTITY,
	product: {
		id
	}
});


export const changeQuantity = (id: string, quantity: string): IChangeQuantity => ({
	type: ActionTypes.CHANGE_QUANTITY,
	product: {
		id,
		quantity
	}
});

export const removeFromCart = (id: string): IRemoveFromCart => ({
	type: ActionTypes.REMOVE_FROM_CART,
	product: {
		id
	}
});

export const handleRemoveFromCart = (id: string) => {
	return (dispatch: any) => {
		dispatch(removeFromCart(id));
		dispatch(putMessage({
			id: uuid(),
			message: 'Product was removed.',
			type: 'info'
		}))
	}
};

export const updateCartValue = (products: IProduct[]): IUpdateCartValue => ({
	type: ActionTypes.UPDATE_CART_VALUE,
	products
});

export const updateBuyerData = (buyer: IBuyer): IUpdateBuyerData => ({
	type: ActionTypes.UPDATE_BUYER_DATA,
	buyer
});