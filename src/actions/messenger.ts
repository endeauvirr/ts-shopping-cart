import IMessage from "../models/Message";
import { ActionTypes } from '../utils/constants';

export interface IPutMessage {
	type: ActionTypes.PUT_MESSAGE,
	message: IMessage
}
export interface IRemoveMessage {
	type: ActionTypes.REMOVE_MESSAGE,
	id: string
}


export type MessengerActionTypes = IPutMessage | IRemoveMessage;



export const putMessage = (message: IMessage): IPutMessage => ({
	type: ActionTypes.PUT_MESSAGE,
	message
});


export const removeMessage = (id: string): IRemoveMessage => ({
	type: ActionTypes.REMOVE_MESSAGE,
	id
});