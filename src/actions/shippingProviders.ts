import IProduct from "../models/Product";
import { ActionTypes } from "../utils/constants";


export interface ISetInitialShippingProvider {
	type: ActionTypes.SET_INITIAL_SHIPPING_PROVIDER,
	products: IProduct[]
}


export type ShippingProvidersActionTypes = ISetInitialShippingProvider;


export const setInitialShippingProvider = (products: IProduct[]): ISetInitialShippingProvider => {
	return {
		type: ActionTypes.SET_INITIAL_SHIPPING_PROVIDER,
		products
	}
};