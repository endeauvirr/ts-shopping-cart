export default interface ICartProgress {
	cartConfirmed: boolean,
	shippingConfirmed: boolean
}