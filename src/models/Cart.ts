import IProduct from "./Product";

export default interface ICart {
	products: IProduct[],
	buyer: any, // TODO, interface IBuyer should be here. TS throws error on initialState object with it. Temp fix.
	cartValue: 0,
	freeShippingAvailable: false,
	areProductsFetched: false
}