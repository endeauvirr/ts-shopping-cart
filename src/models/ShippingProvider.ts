export default interface IShippingProvider {
	id: number,
	name: string,
	value: number,
	selected: boolean,
	disabled: boolean
}