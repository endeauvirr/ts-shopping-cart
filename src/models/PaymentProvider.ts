export default interface IPaymentProvider {
	id: number,
	name: string,
	selected?: boolean,
	disabled?: boolean
}