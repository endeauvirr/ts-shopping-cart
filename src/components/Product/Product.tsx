import * as React from 'react';
import * as styles from './Product.scss';

import { connect } from 'react-redux';
import {
	increaseQuantity,
	decreaseQuantity,
	changeQuantity,
	handleRemoveFromCart
} from "../../actions/cart";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import classNames from 'classnames/bind';

import { formatPrice } from "../../utils/helpers";


const cx = classNames.bind(styles);

interface IProps {
	id: string,
	name: string,
	quantity: number,
	image: string,
	description: string,
	price: any,
	increaseQuantityAction(id: string),
	decreaseQuantityAction(id: string),
	changeQuantityAction(id: string, quantity: string),
	handleRemoveFromCartAction(id: string)
}

class Product extends React.Component <IProps, {}> {
	public render() {
		const { name, image, price, description, quantity } = this.props;
		
		return (
			<div className={styles.productContainer}>
				
				<div className={styles.productNameWrapper}>
					
					<div className={styles.imageWrapper}>
						<img src={image} alt={name} />
					</div>
					<div className={styles.nameWrapper}>
						<h3>{name}</h3>
						<p>{description}</p>
					</div>
				
				</div>
				
				<div className={styles.quantityWrapper}>
					<button
						type="button"
						onClick={this.decreaseQuantity}
						disabled={(quantity === 1)}
					>
						-
					</button>
					<input
						type="text"
						name="quantity"
						value={quantity}
						onChange={this.changeQuantity}
						onFocus={(event) => event.currentTarget.select()}
						className={cx('quantityInput')}
						maxLength={3}
					/>
					<button
						type="button"
						onClick={this.increaseQuantity}
						disabled={(quantity >= 100)}
					>
						+
					</button>
				</div>
				
				<div className={styles.priceWrapper}>
					<p className={styles.price}>
						<span>Unit price:</span>
						{formatPrice(price)}
					</p>
					<p className={cx('price', 'summary')}>
						<span>Summary:</span>
						{formatPrice(price * quantity)}
					</p>
				</div>
				
				<button
					type="button"
					className={styles.removeProduct}
					onClick={this.handleRemoveFromCart}
				>
					<FontAwesomeIcon icon={faTrashAlt} size="lg" />
				</button>
			</div>
		)
	}
	public changeQuantity = (event) => {
		const { id, changeQuantityAction } = this.props;
		
		changeQuantityAction(id, event.currentTarget.value);
	}
	private increaseQuantity = () => {
		const { id, increaseQuantityAction } = this.props;
		
		increaseQuantityAction(id);
	}
	private decreaseQuantity = () => {
		const { id, decreaseQuantityAction } = this.props;
		
		decreaseQuantityAction(id);
	}
	private handleRemoveFromCart = () => {
		const { id, handleRemoveFromCartAction } = this.props;
		
		handleRemoveFromCartAction(id);
	}

}

const mapDispatchToProps = (dispatch) => {
	return {
		increaseQuantityAction: (id) => dispatch(increaseQuantity(id)),
		decreaseQuantityAction: (id) => dispatch(decreaseQuantity(id)),
		changeQuantityAction: (id, quantity) => dispatch(changeQuantity(id, quantity)),
		handleRemoveFromCartAction: (id) => dispatch(handleRemoveFromCart(id))
	}
};

export default connect(null, mapDispatchToProps)(Product);