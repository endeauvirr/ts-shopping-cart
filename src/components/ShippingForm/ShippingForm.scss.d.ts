export const selectControl: string;
export const selectControlArrow: string;
export const formWrapper: string;
export const formField: string;
export const required: string;
export const submitField: string;
export const fieldWrapper: string;
export const phonePrefix: string;
export const errors: string;
export const formSubmit: string;
