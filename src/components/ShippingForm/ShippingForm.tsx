import * as React from 'react';

import * as styles from './ShippingForm.scss';
import * as formStyles from '../../assets/scss/_form-elements.scss';
import classNames from "classnames/bind";

import { Link } from 'react-router-dom'

import { formatShippingPrice } from "../../utils/helpers";

import { withFormik, InjectedFormikProps, Form, Field } from 'formik';
import { object, string } from 'yup';

import IShippingProvider from "../../models/ShippingProvider";
import IBuyer from "../../models/Buyer";


const cx = classNames.bind({...formStyles, ...styles});


interface IFormValues {
	name: string,
	address: string,
	phone: string,
	email: string,
	shippingProviders: string
}

interface IFormProps {
	name?: string,
	address?: string,
	phone?: string,
	email?: string,
	shippingProviders: IShippingProvider[],
	buyer: IBuyer,
	freeShippingAvailable: boolean,
	history: any,
	updateBuyerData(IBuyer),
	handleProceedToNextStep()
}





const ShippingForm: React.SFC<InjectedFormikProps<IFormProps, IFormValues>> = ({ values, errors, touched, isSubmitting, shippingProviders, freeShippingAvailable }) => {
	return (
		<Form className={styles.formWrapper}>
			<div className={cx('formField', 'required')}>
				<label htmlFor='name'>Name:</label>
				
				<div className={styles.fieldWrapper}>
					<Field type="text" name="name" />
					{ touched.name && errors.name && <p className={styles.errors}>{ errors.name }</p> }
				</div>
			</div>
			
			<div className={cx('formField', 'required')}>
				<label htmlFor='address'>Address:</label>
				
				<div className={styles.fieldWrapper}>
					<Field type="text" name="address" />
					{ touched.address && errors.address && <p className={styles.errors}>{ errors.address }</p> }
				</div>
			</div>
			
			<div className={styles.formField}>
				<label htmlFor='phone'>Phone:</label>
				
				<div className={styles.fieldWrapper}>
					<div className={styles.phonePrefix}>
						<span>+48</span>
						<Field type="tel" name="phone" maxLength='9' />
					</div>
					{ touched.phone && errors.phone && <p className={styles.errors}>{ errors.phone }</p> }
				</div>
			</div>
			
			<div className={cx('formField', 'required')}>
				<label htmlFor='email'>E-mail:</label>
				
				<div className={styles.fieldWrapper}>
					<Field type="email" name="email" />
					{ touched.email && errors.email && <p className={styles.errors}>{ errors.email }</p> }
				</div>
			</div>
			
			<div className={styles.formField}>
				<label >Shipping method:</label>
				
				<div className={cx('fieldWrapper')}>
					<div className={cx('selectControl')}>
						<Field component="select" name="shippingProviders">
							{
								shippingProviders.map(provider => {
									return (
										!provider.disabled &&
										<option
											key={provider.id}
											value={provider.id}
										>
											{provider.name} - {freeShippingAvailable ? 'Free' : formatShippingPrice(provider.value)}
										</option>
									)
								})
							}
						</Field>
						<div className={cx('selectControlArrowcx')}>&nbsp;</div>
					</div>
				</div>
			</div>
			
			<div className={cx('formField', 'submitField')}>
				<Link to='/cart'>back to cart</Link>
				<button type="submit" className={styles.formSubmit}>PAY</button>
			</div>
		</Form>
	);
};


const FormikShippingForm = withFormik<IFormProps, IFormValues>({
	mapPropsToValues: ({ name, address, phone, email, shippingProviders, buyer }) => {
		const activeShippingProvider: any = shippingProviders.filter(provider => {
			return (!provider.disabled)
		});
		
		return {
			name: buyer.name || '',
			address: buyer.address || '',
			phone: buyer.phone || '',
			email: buyer.email || '',
			shippingProviders: buyer.shippingProviderId || activeShippingProvider.shift().id.toString()
		}
	},
	validationSchema: object().shape({
		name: string().required('Please provide name').min(3, 'Name should be at least 3 chars').trim(),
		address: string().required('Please provide address').trim(),
		email: string().required('Please provide e-mail address').email('Email format is invalid. Valid example: example@provider.com').trim()
	}),
	validate: (values) => {
		const errors: any = {};
		const { phone } = values;
		
		if (phone.length > 0 && phone.length < 9) {
			errors.phone = 'Phone must contain 9 digits';
		} else if (!/^[0-9]*$/g.test(phone)) {
			errors.phone = 'Only numbers are allowed';
		}
		
		return errors;
	},
	handleSubmit: (values, FormikBag) => {
		const { updateBuyerData, handleProceedToNextStep } = FormikBag.props;
		const { name, address, email, phone, shippingProviders:shippingProviderId } = values;
		
		updateBuyerData({
			name: name.trim(),
			address: address.trim(),
			email: email.trim(),
			phone,
			shippingProviderId
		});
		
		handleProceedToNextStep();
	}
})(ShippingForm);


export default FormikShippingForm;