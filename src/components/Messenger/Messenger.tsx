import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as styles from './Messenger.scss';

import Message from '../Message/Message';
import IMessage from "../../models/Message";

const messengerRoot: any = document.getElementById('messenger-root'); // TODO, don't know what type should be here :/

interface IProps {
	messages: IMessage[],
	removeMessage(id: string)
}

const Messenger = (props: IProps) => {
	const { messages, removeMessage } = props;
	
	return ReactDOM.createPortal(
		<div className={styles.messengerWrapper}>
			{
				messages.map(message => (
					<Message message={message} key={message.id} removeMessage={removeMessage} />
				))
			}
		</div>
		, messengerRoot)
};

export default Messenger;