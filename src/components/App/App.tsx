import * as React from 'react';
import AppRouter from '../../routes';
import * as styles from './App.scss';

import { connect } from 'react-redux';
import { removeMessage } from "../../actions/messenger";
import Messenger from '../Messenger/Messenger';
import {IMessenger} from "../../reducers/messenger";


interface IProps {
	messenger: IMessenger,
	removeMessageAction(id: string)
}


const App = (props: IProps) => {
	const { messengerActive, messages } = props.messenger;
	
	return (
		<React.Fragment>
			{
				messengerActive && <Messenger messages={messages} removeMessage={props.removeMessageAction} />
			}
			
			<div className={styles.container}>
				<AppRouter />
			</div>
		</React.Fragment>
	)
};

const mapStateToProps = ({ messenger }) => {
	return {
		messenger
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		removeMessageAction: (id) => dispatch(removeMessage(id))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
