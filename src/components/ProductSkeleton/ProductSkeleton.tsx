import * as React from 'react';

import * as productStyles from '../Product/Product.scss';
import * as skeletonStyles from './ProductSkeleton.scss';

import classNames from "classnames/bind";

const cx = classNames.bind({...productStyles, ...skeletonStyles});

const ProductSkeleton = () => {
	return (
		<div className={cx('productContainer', 'skeleton')}>
			
			<div className={cx('productNameWrapper')}>
				
				<div className={cx('imageWrapper')}>
					<span className={cx('contentPlaceholder')} style={{height: '100px', width: '100px'}}>loading image</span>
				</div>
				<div className={cx('nameWrapper')}>
					<h3 className={cx('contentPlaceholder')}>loading header</h3>
					<p className={cx('contentPlaceholder')}>loading description</p>
				</div>
			
			</div>
			
			<div className={cx('quantityWrapper')} style={{width: '150px'}}>
				<span className={cx('contentPlaceholder')}>loading quantity</span>
			</div>
			
			<div className={cx('priceWrapper')}>
				<span className={cx('contentPlaceholder')}>loading price</span>
			</div>
		</div>
	);
};

export default ProductSkeleton;