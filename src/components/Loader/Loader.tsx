import * as React from 'react';
import * as styles from './Loader.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons';

const Loader = () => {
	return (
		<div className={styles.loaderWrapper}>
			<FontAwesomeIcon icon={faCircleNotch} spin={true} size="3x"/>
		</div>
	)
}


export default Loader;
