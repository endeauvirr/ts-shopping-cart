import * as  React from 'react';

import Product from '../../components/Product/Product';
import IProduct from "../../models/Product";

interface IProps {
	products: IProduct[]
}

const Products = (props: IProps) => {
	const { products } = props;
	
	return (
		<React.Fragment>
			{
				products.map(({id, ...product }) => {
					return (
						<Product key={id} id={id} {...product} />
					)
				})
			}
		</React.Fragment>
	);
};


export default Products;