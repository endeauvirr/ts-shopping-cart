import * as React from 'react';

import * as AppStyles from '../App/App.scss';
import * as styles from './Message.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import className from 'classnames/bind';
import IMessage from "../../models/Message";

const cx = className.bind({...AppStyles, ...styles});

interface IProps {
	message: IMessage,
	removeMessage(id: string)
}

const Message = (props: IProps) => {
	const { removeMessage } = props;
	const { id, type, message } = props.message;
	
	setTimeout(() => {
		removeMessage(id)
	}, 5000);
	
	return (
		<div className={cx('message', `type${type}`)}
		>
			<div className={cx('container', 'messageContainer')}>
				<p>
					{message}
				</p>
				
				<button
					type="button"
					onClick={() => removeMessage(id)}
				>
					<FontAwesomeIcon icon={faTimes} size='lg' />
				</button>
			</div>
		</div>
	);
};

export default Message;