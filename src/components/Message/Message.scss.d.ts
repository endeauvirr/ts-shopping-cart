export const message: string;
export const messageContainer: string;
export const typeok: string;
export const typewarn: string;
export const typeerror: string;
export const typeinfo: string;
