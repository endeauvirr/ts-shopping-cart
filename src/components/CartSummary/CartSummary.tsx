import * as React from 'react';
import * as styles from './CartSummary.scss';

interface IProps {
	products: IProduct[]
}

import { calculateCartValue } from "../../utils/helpers";
import IProduct from "../../models/Product";

const CartSummary = (props: IProps) => {
	const { products } = props;
	
	return (
		<React.Fragment>
			<p className={styles.summary}>
				<span>Grand total</span>
				{ calculateCartValue(products) }
			</p>
		</React.Fragment>
	);
};

export default CartSummary;