import { CartProgressActionTypes } from "../actions/cartProgress";
import ICartProgress from "../models/CartProgress";
import { ActionTypes } from '../utils/constants';


const initialState: ICartProgress = {
	cartConfirmed: false,
	shippingConfirmed: false
};

const shoppingProgress = (state = initialState, action: CartProgressActionTypes) => {
	switch(action.type) {
		case ActionTypes.UPDATE_CART_PROGRESS : {
			return {
				...state,
				...action.status
			}
		}
		default : {
			return state;
		}
	}
};

export default shoppingProgress;