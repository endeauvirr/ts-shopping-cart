import { MessengerActionTypes } from "../actions/messenger";
import IMessage from "../models/Message";
import { ActionTypes } from '../utils/constants';


export interface IMessenger {
	messages: IMessage[],
	messengerActive: boolean
}


const initialState: IMessenger = {
	messengerActive: false,
	messages: []
};

export const messenger = (state = initialState, action: MessengerActionTypes) => {
	switch(action.type) {
		case ActionTypes.PUT_MESSAGE : {
			const { message } = action;
			const { messages } = state;
			
			if (messages.length === 0) {
				return {
					messengerActive: true,
					messages: [...messages, message]
				}
			}
			
			return {
				...state,
				messages: [...messages, message]
			}
		}
		case ActionTypes.REMOVE_MESSAGE : {
			const { id } = action;
			const { messages } = state;
			
			const updatedMessages = messages.filter(currentMessage => currentMessage.id !== id);
			
			if (updatedMessages.length === 0) {
				return {
					messengerActive: false,
					messages: updatedMessages
				}
			}
			
			return {
				...state,
				messages: updatedMessages
			}
		}
		default : {
			return state;
		}
	}
};

export default messenger;