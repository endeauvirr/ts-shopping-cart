import { combineReducers } from 'redux';

import cart from '../reducers/cart';
import cartProgress from '../reducers/cartProgress';
import messenger from '../reducers/messenger';
import paymentProviders from '../reducers/paymentProviders';
import shippingProviders from '../reducers/shippingProviders';

export default combineReducers({
	cart,
	cartProgress,
	messenger,
	paymentProviders,
	shippingProviders,
});
