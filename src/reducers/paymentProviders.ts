import { PaymentProvidersActionTypes } from "../actions/paymentProviders";
import IPaymentProvider from "../models/PaymentProvider";
import { ActionTypes } from '../utils/constants';


const initialState: IPaymentProvider[] = [
	{
		id: 1,
		name: 'Pay7',
		selected: true
	},
	{
		id: 2,
		name: 'Cash payment'
	}
];

const paymentProviders = (state = initialState, action: PaymentProvidersActionTypes) => {
	switch(action.type) {
		case ActionTypes.SET_PAYMENT_PROVIDER : {
			const { id } = action;
			
			return state.map(provider => {
				if (provider.id === id) {
					provider.selected = true;
					return provider
				}
				
				provider.selected = false;
				return provider;
			})
			
		}
		default : {
			return state;
		}
	}
};


export default paymentProviders;