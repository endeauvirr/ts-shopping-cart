import { ShippingProvidersActionTypes } from "../actions/shippingProviders";
import IShippingProvider from "../models/ShippingProvider";
import { ActionTypes } from '../utils/constants';


const initialState: IShippingProvider[] = [
	{
		id: 1,
		name: 'ninjPost',
		value: 0,
		selected: false,
		disabled: false
	},
	{
		id: 2,
		name: 'D7L',
		value: 15.99,
		selected: false,
		disabled: false
	},
	{
		id: 3,
		name: '7post',
		value: 7.99,
		selected: false,
		disabled: false
	}
];

const shippingProviders = (state = initialState, action: ShippingProvidersActionTypes) => {
	switch(action.type) {
		case ActionTypes.SET_INITIAL_SHIPPING_PROVIDER : {
			const { products } = action;
			
			const isNinjPostDisabled = products.length > 3;
			
			return state.map(provider => {
				
				if (provider.id === 1) {
					return {
						...provider,
						disabled: isNinjPostDisabled
					}
				}
				
				return {
					...provider
				}
			});
		}
		default : {
			return state;
		}
	}
};

export default shippingProviders;